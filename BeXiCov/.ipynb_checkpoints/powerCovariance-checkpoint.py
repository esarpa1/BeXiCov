'''module to comute the covariance matrix of observed Powerspectrum multipoles

    functions are dived in 2 categories:
        - Theoretical covariance
        - BestFit covariance
    note: GaussianCovariance implementation from COMET
'''
         
#==== import needed libraries =====
import BeXiCov.Models as Models
from iminuit import Minuit
from scipy.linalg import cholesky, solve_triangular
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from BAOFit.BAOemulator import load_emulator, get_emu_predictions
from BAOFit.powerFit import BestFit


#===== Theoretical covariance

def ThCovariance(k, volume, number_density, cosmo, space, rectype='', sigma_rec = 0, now_opt = True, FoG_opt=False):

    #
    '''
        routine to compute P(k) Gaussian covariance Grieb et al 2016) given the expected non clustering signal
        GaussianCovariance implementation from COMET. 
    
        k: array of separation bins
        volume: (float) survey volume
        number_density: sample number density
        cosmo: list of cosmological parameters, example in Models.DefaultCosmology()
        space: 'RealSpace' if the template has to be computed in real-space,
                'RedshiftSpace' if in redshift-space
        rectype: string of reconstruction type. leave empty for no-reconstruction,
                 'rec-sym', 'rec-iso' for the Zel'dovich reconstruction without and
                  with RSD removal
        sigma_rec: kernel size of rec-iso, default is 0.
        FoG_opt: boolean option for FoG default is false
        now_opt: boolean option for FoG no wiggle implementation. If False the P(k) is directly multiplied by the dumping function is false, 
         The reference model in real space is set to P(k,mu) = [P_lin(k) - P_nw(k)] * Damping + P_nw(k), default is True
        returns:
            xy, yx = np.meshgrid(sconc, sconc, indexing='ij'),
            covariance matrix,
            corr matrix
            Phpar: dictionary of parameters used for the theoretical covariance
    '''

    
    print('sigma rec th', sigma_rec)
    kh, Pk_l, f, sigma8,T_cmb = Models.Pk_linear(cosmo)
    Pk_now = Models.Pk_nowiggle(kh,Pk_l,cosmo,sigma8,T_cmb)
    #built Pk model
    if (rectype == ''):
        sigma_2 = Models.Sigma_pre_rec(kh, Pk_l)
    else: sigma_2 = Models.Sigma_post_rec(kh, Pk_l)

    if space == 'RedshiftSpace':
        sigma_par_2, sigma_per_2 = Models.Sigma_par_per(sigma_2,f)
    else: sigma_par_2 = sigma_per_2 = sigma_2

    if FoG_opt: Sigma_s = np.sqrt(Models.Sigma_pre_rec(kh, Pk_l))
    else: Sigma_s = 0

    
    Phpar={
    'alpha_par': 1, #AP shift along the l.o.s
    'alpha_perp': 1, #AP shift perpendicular to the l.o.s
    'bias': cosmo['bias'], #linear bias
    'f':f, #growth rate of structures
    'Sigma_par': np.sqrt(sigma_par_2), #non-linear damping perpendicular along the l.o.s
    'Sigma_perp':  np.sqrt(sigma_per_2), #non-linear damping perpendicular to the l.o.s
    'Sigma_s':Sigma_s, #velocity dispersion parameter, set 0 if FoG = False
    'Sigma_rec': 0 #size Zel'dovich reconstruction gaussian kernel
    }
    if rectype == 'rec-iso': Phpar_pre['Sigma_rec'] = sigma_rec

    Pk_BAO_template = model_Pk_ell(k,Phpar,k, Pk_l, Pk_now)
    if now_opt: Pk_BAO_template = model_Pk_ell(k,Phpar,k, Pk_l, Pk_now)
    else: Pk_BAO_template = model_Pk_ell(k,Phpar,k, Pk_lin, np.zeros(k.shape))

         
    #set output bins
    dlnk = np.diff(np.log(k))
    index = np.linspace(0,len(k)-1,len(k))
    dk = np.min(k) * (np.exp(index + 0.5 dlnk) - np.exp(index - 0.5 dlnk))
    
    l_list = [0, 2, 4] #multiple list

    cov = Gaussian_covariance(k, Pk_BAO_template, l_list, dk, number_density, volume)
 
    std = np.sqrt(np.diagonal(cov))
    corr = cov / np.outer(std, std)
    kconc=np.concatenate((k,k,k))
    xy, yx = np.meshgrid(kconc, kconc,indexing='ij')
    
   # print('checking properties of theoretical covariance')
   # print("Is symmetric:", is_symmetric(cov))
   # print("Is positive semidefinite:", is_positive_semidefinite(cov))
    
    return xy,yx,cov,corr, Phpar

def is_symmetric(matrix):
    return np.allclose(matrix, matrix.T)

def is_positive_semidefinite(matrix):
    eigenvalues, _ = np.linalg.eig(matrix)
    return np.all(eigenvalues >= 0)

#====== BestFit covariance ======

#class to obtain an estimate of the covariance matrix reproducing the data
class BestFitCovariance:
    '''
        class to compute power spectrum Gaussian covariance (Grieb et al 2016) from the best fit of the data
        GaussianCovariance implementation from COMET 
                
        k: array of separation bins
        Pk_elles: (3,len(k)): array measured power spectrum multipoles (monopole,quadrupole,hexadecapole)
        volume: (float) survey volume
        number_density: sample number density
        cosmo: list of cosmological parameters, example in Models.DefaultCosmology()
        space: 'RealSpace' if the template has to be computed in real-space,
                'RedshiftSpace' if in redshift-space
        rectype: string of reconstruction type. leave empty for no-reconstruction,
                 'rec-sym', 'rec-iso' for the Zel'dovich reconstruction without and
                  with RSD removal
        sigma_rec: smoothing kernel for rec-iso, default is 0
        ellmax: maximum order of even multipoles to consider for the fit, default is 2
        NormCov: normalisation factor to appliy to the covariance beore best fit, default is 1
        Niter: number of iterations, default is 2
        initial_guess=None: #touple of values for best fit initial guess
        FoG_opt: boolean option for FoG default is false
        now_opt: boolean option for FoG no wiggle implementation. If False the P(k) is directly multiplied by the dumping function is false, 
         The reference model in real space is set to P(k,mu) = [P_lin(k) - P_nw(k)] * Damping + P_nw(k), default is True
        returns:
        
        
            xy, yx = np.meshgrid(sconc, sconc, indexing='ij'),
            covariance matrix,
            corr matrix
    '''
    
    def __init__(self, k, Pk_ell_data,volume, number_density, cosmo, space, rectype='',sigma_rec=0., ellemax=2,NormCov=1, Niter=2, initial_guess=None, now_opt = True, FoG_opt = False):    
        self.k=k
        self.Pk_ell_data = Pk_ell_data
        self.volume = volume
        self.number_density = number_density
        self.cosmo = cosmo
        self.space = space
        self.rectype = rectype
        self.NormCov = NormCov
        self.fit_range = fit_range
        self.ellemax = ellemax #adding exadecapole
        self.Niter = Niter
    
        self.initial_guess=initial_guess
        self.sigma_rec = 0
        if rectype == 'rec-sio': self.sigma_rec = sigma_rec
        self.FoG_opt = FoG_opt
        self.now_opt = now_opt

        self.fit_range = [np.min(k),np.max(k)]
        print('fit-range',self.fit_range)       
        
    def run(self):
    
        #get theoretical covariance
        print('generating theoretial covariance ...')
        self.xy,self.yx,self.covTH = self.GetThCov()
        #self.test_thcov()
        #initial guess, can be modified in function
        if self.initial_guess == None:
            initial_guess = self.Phar_th
        else: initial_guess=self.initial_guess
        print('performing best fit 1st iteration ...') #use Bexicov

        BAOFit = BestFit(k, self.Pk_ell_data, self.covTH/self.NormCov, self.cosmo, self.space, rectype = self.rectype, sigma_rec=self.sigma_rec, ellemax=self.ellemax, fit_range=self.fit_range, initial_guess=self.initial_guess, FoG_opt=self.FoG_opt)
        
        self.best_fit_params,self.param_errors= BAOFit.run()
        print('construct covariance, 1st iteration...')
        xy,yx,self.cov_it,self.corr_it = self.ConstructCovariance(BAOFit, self.best_fit_params)
        
        for n in range (1,self.Niter):
            print('performing best fit',str(n), 'iteration ...')
            BAOFit = BestFit(k, self.Pk_ell_data, self.cov_it/self.NormCov, self.cosmo, self.space, rectype = self.rectype, sigma_rec=self.sigma_rec, ellemax=self.ellemax, fit_range=self.fit_range, initial_guess=self.initial_guess, FoG_opt=self.FoG_opt)
        
            self.best_fit_params,self.param_errors= BAOFit.run()
            
            xy,yx,self.cov_it,self.corr_it = self.ConstructCovariance(BAOFit, self.best_fit_params)
    

        return
        
        
        
        
    #==== intermediate routines
    def GetThCov(self):
        '''
            get theporetical covariance to obtain best fit
        '''
        xy,yx,cov,corr, self.Phar_th =  ThCovariance(self.s, self.volume, self.number_density, self.cosmo, self.space, rectype=self.rectype, sigma_rec=self.sigma_rec, ellemax=self.ellemax,now_opt = self.now_opt, FoG_opt = self.FoG_opt)
        #plt.imshow(corr,origin='lower')
       # plt.show()
     
        return xy,yx,cov
    
     
    def ConstructCovariance(self,BAOFit,par):

        #Get model 
        self.Pk_BAO_BF= BAOFit.GetModel(self.k,par)
        #set output bins
        dlnk = np.diff(np.log(self.k))
        index = np.linspace(0,len(self.k)-1,len(self.k))
        dk = np.min(self.k) * (np.exp(index + 0.5 dlnk) - np.exp(index - 0.5 dlnk))
        
        l_list = [0, 2, 4] #multiple list
    
        cov = Gaussian_covariance(k, self.Pk_BAO_BF, l_list, dk, self.number_density, self.volume)
     
        std = np.sqrt(np.diagonal(cov))
        corr = cov / np.outer(std, std)
        kconc=np.concatenate((k,k,k))
        xy, yx = np.meshgrid(kconc, kconc,indexing='ij')
        
       # print('checking properties of theoretical covariance')
       # print("Is symmetric:", is_symmetric(cov))
       # print("Is positive semidefinite:", is_positive_semidefinite(cov))
        
        return xy,yx,self.covBF,self.corrBF
        
        
    def show_tests(self):
        '''
            show the goodness of the procedure
            - plotBF: overplots  the results of the best fit on top of the data
            - Plot_CovModel_test: plots the hankle of the fiducial Pk used to build BF covaria
            

        '''
        
        print('best fit model ...')
        self.plotBF(self.k,self.Pk_ell_data,self.covTH/self.NormCov,self.Pk_BAO_BF)
        print('hankle transform of fiducial Power-spectrum used to build the covariance ...')
        self.Plot_CovModel_test()
        print('comparison between theoretical and best fit covariance ...')
        self.PlotCovComparison(self.covTH,self.covBF)
        return
        
    def test_thcov(self):
        print('testing fiducial model for theoretical covariance...')
        xi_elles= Models.xi_ell_models(self.s, self.cosmo, self.space, self.rectype, self.sigma_rec)
        self.plotBF(self.s,self.xi_ell_data,self.covTH/self.NormCov,xi_elles)
        return
        
    def plotBF(self,k,Pk_ell_data,cov,Pk_bf):
        '''
            overplots  the results of the best fit on top of the data
            k: separation array
            Ok_ell_data: Pk multipoles
            cov: covariance used for the fit
            Pk_bf: best fit model for Pk multipoles
            
        '''
        
        std=np.sqrt(np.diag(cov)).reshape(3,len(k))
        
        color=['#009ffd','#ffa400','g']
       
        ylabels=['$r^2\\xi_0~(h^{-1}\mathrm{Mpc})$','$r^2\\xi_2~(h^{-1}\mathrm{Mpc})$','$r^2\\xi_4~(h^{-1}\mathrm{Mpc})$']

        labels=['$r^2\\xi_0$','$r^2\\xi_2$','$r^2\\xi_4$']
        labelsM=['$r^2\\xi^\mathrm{BF}_\mathrm{0}$','$r^2\\xi^\mathrm{BF}_\mathrm{2}$','$r^2\\xi^\mathrm{BF}_\mathrm{4}$']
        alph=0.2
        ft=12
        figure, axs = plt.subplots(nrows=3, ncols=1, sharex='col', sharey='row',figsize=(6, 6))


        smin=0.01
        smax=200
        for m in range (0,3):
            axs[m].set_ylabel(ylabels[m],fontsize=ft)
            axs[m].axhline(0,ls=':',lw=.5,c='k')
            axs[m].tick_params(labelsize=ft-3)

            axs[m].plot(s,xi_ell_data[m]*s**2,c=color[m],alpha=1,lw=1.5,label=labels[m])
            axs[m].fill_between(s,(xi_ell_data[m]-std[m])*s**2,\
                                        (xi_ell_data[m]+std[m])*s**2,color=color[m],alpha=0.1)

         
            axs[m].plot(s[(s<smax)&(s>smin)],xi_bf[m][(s<smax)&(s>smin)]\
                         *s[(s<smax)&(s>smin)]**2,c='k',alpha=1,lw=0.8,ls='--',label=labelsM[m])
           


        axs[0].legend(loc="upper right",fontsize=ft-2,frameon=False)
        axs[2].set_xlabel('$r~(h^{-1}\mathrm{Mpc})$',fontsize=ft)
        plt.tight_layout()
        figure.subplots_adjust(hspace=0)
        plt.show()
        
        return
        
    def Plot_CovModel_test(self):
        '''
            plots the hankle of the fiducial Pk used to build BF covariance
        '''
    
        rtemp, xi_mu_r=Models.ximur(self.kh,self.P_mu_k_all)
        par_temp={'alpha_par':1.,'alpha_perp':1}
        xi_l_ref=Models.xiell(self.s,par_temp,xi_mu_r)
        self.plotBF(self.s,self.xi_ell_data,self.covTH/self.NormCov,xi_l_ref)
        
        return
        
    def get_correlation_matrix(self,covariance_matrix):
        # Compute the standard deviations of each variable
        std_devs = np.sqrt(np.diag(covariance_matrix))
        
        # Compute the correlation coefficients
        correlation_matrix = covariance_matrix / np.outer(std_devs, std_devs)
        
        return correlation_matrix
        
    def PlotCovComparison(self,xicov_th,xicov_BF):
        '''
            plots comparioson between theoretical covariance and best fit covatiance
            xicov_th: theoretical covariance
            xicov_BF: best fit covatiance
        '''
        ft=12
        fig, axs = plt.subplots(nrows=3, ncols=1, sharex='col', sharey='row',figsize=(5,7))
        colors=['#21b0fe','#fe218b','#fed700']
        labels=['mock','th','it']

        yaxis=['$s^2Cov_{ii,0}~(\mathrm{Mpc}h^{-1})^2$','$s^2Cov_{ii,2}~(\mathrm{Mpc}h^{-1})^2$',\
        '$s^2Cov_{ii,4}~(\mathrm{Mpc}h^{-1})^2$']

        diag_it=np.diag(xicov_BF)
        diag_th=np.diag(xicov_th)
        subarray_size = len(diag_it) // 3

       
        errxi_th=np.zeros((3,subarray_size))
        errxi_it=np.zeros((3,subarray_size))

        errxi_th[0]=diag_th[:subarray_size]
        errxi_th[1]=diag_th[subarray_size:2*subarray_size]
        errxi_th[2]=diag_th[2*subarray_size:]
        errxi_it[0]=diag_it[:subarray_size]
        errxi_it[1]=diag_it[subarray_size:2*subarray_size]
        errxi_it[2]=diag_it[2*subarray_size:]
        s=np.linspace(0,200,len(errxi_th[0]))


        for i in range (0,3):
            axs[i].plot(s,s**2*errxi_th[i],ls='--',color=colors[1],label=labels[1])
            axs[i].plot(s,s**2*errxi_it[i],ls='--',color=colors[2],label=labels[2])
            axs[i].set_ylabel(yaxis[i],fontsize=ft)
            axs[i].tick_params(labelsize=ft)
        axs[0].legend(loc="upper left",fontsize=ft,frameon=False)
        axs[2].set_xlabel('$s~(\mathrm{Mpc}h^{-1})$',fontsize=ft)

        plt.tight_layout()
        fig.subplots_adjust(hspace=0)
        fig.subplots_adjust(wspace=0)

        plt.show()
        
        fig, axs = plt.subplots(1, 2,sharex=True, sharey=True,figsize=(10,6))
        fig.subplots_adjust(hspace=0)
        fig.subplots_adjust(wspace=0)
        xicorr_th = self.get_correlation_matrix(xicov_th)
        xicorr_it = self.get_correlation_matrix(xicov_BF)
        im1 = axs[0].imshow(xicorr_th, cmap='coolwarm',vmin=-1,vmax=1,origin='lower')
        im2 = axs[1].imshow(xicorr_it, cmap='coolwarm',vmin=-1,vmax=1,origin='lower')
     

        axs[0].set_title('Th')
        axs[1].set_title('It')


        # Create a common colorbar for both subplots
        cbar = fig.colorbar(im1, ax=axs, shrink=0.6)
        cbar.set_label('Corr')

        plt.show()
        return
    

    

#===== Gaussian covariance - from Comet
'''Copyright (c) 2018 The Python Packaging Authority

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.'''


def Gaussian_covariance(k, Pk_ell, ell, dk, nbar, volume, Nmodes = None):
    ''' Compute the Gaussian covariance of the given power spectrum multipoles.


        Parameters
        ----------
        k: float or list or numpy.ndarray
            Wavemodes :math:`k` at which to evaluate the multipoles. If a list
            is passed, it has to match the size of `ell`, and in that case
            each wavemode refer to a given multipole.
        Pk_ell: array of float, shape (len(ell),len(k): Pk multipoles
   
        ell: int or list
            pecific multipole order :math:`\ell`.
            Can be chosen from the list [0,2,4,6], whose entries correspond to
            monopole (:math:`\ell=0`), quadrupole (:math:`\ell=2`),
            hexadecapole (:math:`\ell=4`) and octopole (:math:`\ell=6`).
      
        dk: float
            Width of the :math:`k` bins.
            
        nbar: mean number density if the sample in Mpc/h
        
        volume: float, Reference volume to be used in the calculation of the gaussian
       
        Nmodes: numpy.ndarray, optional
            Number of fundamental modes per :math:`k-shell. The size of the
            array should match the size of ``k``. Defaults to
            :math:`4\pi/3\,\left[(k+\Delta k/2)^3 - (k-\Delta k/2)^3\right]
            /k_f^3`, where :math:`k_f^3 = (2 \pi)^3/V`.
      
        Returns
        -------
        cov: numpy.ndarray
            Gaussian covariance of the selected power spectrum multipoles.
      '''
    
        ell = [ell] if not isinstance(ell, list) else ell
        if not isinstance(k, list):
            k = [np.array(k)]*len(ell)
        elif isinstance(k, list) and len(k) != len(ell):
            raise ValueError(If k is given as a list, it must match the 
                             length of ell.)
                             
        else:
            k = [np.array(x) for x in k]

        nbins = [x.shape[0] for x in k]
        cov = np.zeros([sum(nbins), sum(nbins)])

        k_all = np.unique(np.hstack(k))

        ell_for_cov = ell

        #construct dictionaries of Pell
        l=0
        Pell = {}
        while l < len(np.array(ell)):
            name = 'ell' + ell[l]
            Pell[name] = Pk_ell[l]
        Pell['ell0'] += 1.0/nbar

        

        for i, l1 in enumerate(ell):
            for j, l2 in enumerate(ell):
                if j >= i:
                    kij, id1, id2 = np.intersect1d(k[i], k[j],
                                                   return_indices=True)
                    ids_ij = np.intersect1d(k_all, kij, return_indices=True)[1]
                    cov_l1l2 = Gaussian_covariance_l1l2(
                        l1, l2, k_all, dk, Pell, volume, Nmodes)[ids_ij]
                    cov[sum(nbins[:i]):sum(nbins[:i+1]),
                        sum(nbins[:j]):sum(nbins[:j+1])][id1, id2] = cov_l1l2
                else:
                    cov[sum(nbins[:i]):sum(nbins[:i+1]),
                        sum(nbins[:j]):sum(nbins[:j+1])] = \
                        cov[sum(nbins[:j]):sum(nbins[:j+1]),
                            sum(nbins[:i]):sum(nbins[:i+1])].T

        return cov

def Gaussian_covariance_l1l2(l1, l2, k, dk, Pell, volume,Nmodes=None):
        r"""Compute the gaussian covariance of the power spectrum multipoles.

        Returns the gaussian covariance predictions for the specified power
        spectrum multipoles (of order :math:`\ell_1` and :math:`\ell_2`), at
        the specified wavemodes :math:`k`, and for the given volume.

        Parameters
        ----------
        l1: int
            Order of first power spectrum multipole.
        l2: int
            Order of second power spectrum multipole.
        k: numpy.ndarray
            Wavemodes :math:`k` at which to evaluate the gaussian covariance.
        dk: float
            Width of the :math:`k` bins.
        Pell: dict
            Dictionary containing the monopole, quadrupole and hexadecapole
            of the power spectrum.
        volume: float
            Reference volume to be used in the calculation of the gaussian
            covariance.
        Nmodes: numpy.ndarray, optional
            Number of modes contained in each :math:`k` bin. If not provided,
            its calculation is carried out based on the value of :math:`k` and
            :math:`\mathrm{d}k`. Defaults to **None**.

        Returns
        -------
        cov: numpy.ndarray
            Gaussian covariance of the selected power spectrum multipoles.
        """
        if Nmodes is None:
            Nmodes = volume/3.0/(2.0*np.pi**2)*((k+dk/2.0)**3 - (k-dk/2.0)**3)

        if not self.real_space:
            P0 = Pell['ell0']
            P2 = Pell['ell2']
            P4 = Pell['ell4']

            if l1 == l2 == 0:
                cov = P0**2 + 1.0/5.0*P2**2 + 1.0/9.0*P4**2
            elif l1 == 0 and l2 == 2:
                cov = (2.0*P0*P2 + 2.0/7.0*P2**2 + 4.0/7.0*P2*P4 +
                       100.0/693.0*P4**2)
            elif l1 == l2 == 2:
                cov = (5.0*P0**2 + 20.0/7.0*P0*P2 + 20.0/7.0*P0*P4 +
                       15.0/7.0*P2**2 + 120.0/77.0*P2*P4 + 8945.0/9009.0*P4**2)
            elif l1 == 0 and l2 == 4:
                cov = (2.0*P0*P4 + 18.0/35.0*P2**2 + 40.0/77.0*P2*P4 +
                       162.0/1001.0*P4**2)
            elif l1 == 2 and l2 == 4:
                cov = (36.0/7.0*P0*P2 + 200.0/77.0*P0*P4 + 108.0/77.0*P2**2 +
                       3578.0/1001.0*P2*P4 + 900.0/1001.0*P4**2)
            elif l1 == l2 == 4:
                cov = (9.0*P0**2 + 360.0/77.0*P0*P2 + 2916.0/1001.0*P0*P4 +
                       16101.0/5005.0*P2**2 + 3240.0/1001.0*P2*P4 +
                       42849.0/17017.0*P4**2)
        else:
            cov = Pell['ell0']**2


        cov *= 2.0/Nmodes

        return cov

