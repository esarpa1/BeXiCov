# BeXiCov: Best-fit Xi Covariance Matrix

**BeXiCov** is a Python package that constructs the semi-analytical Gaussian covariance matrix for the measured multipoles of the two-point correlation function (2PCF) pre and post-reconstruction. It was developed for the DR1-Euclid forecast on BAO measurements (Sarpa et al., in prep)

The code has been upgraded allowing the integration of a 2PCF multipoles emulator, speeding up the covariance generation.
The emulator is created and trained using the BAOFitter routine available at https://gitlab.com/esarpa1/BAOFit/

**When using the code, please cite: Sarpa et al 2024: Euclid Consortium: Baryonic Acoustic Oscillations (BAOs) extraction techniques: comparison and optimisation**
## Modules

- **Models.py**: Contains the routines used to compute the power spectrum and correlation function models.
- **Covariance.py**: Generates the theoretical and best-fit covariance matrix reproducing the data.

## Inputs

- Measured two-point correlation function multipoles.
- Fiducial cosmology.
- Survey volume.
- Survey mean number density.
- `rectype`:
  - `''`: Pre-reconstruction.
  - `'rec-iso'`: Zel'dovich post-reconstruction with RSD removal.
  - `'rec-sym'`: Zel'dovich post-reconstruction without RSD removal.
- `space`: `'RealSpace'` or `'RedshiftSpace'`.

## Outputs

- Covariance matrix of the two-point correlation function multipoles evaluated at the data separation vector.

## Installation

1. Clone the BeXiCov repository
2. cd GaussianCovariance 
   - pip insall.
3. Install Camb via: conda install -c conda-forge camb
4. Install Hankl via: pip install hankl
5. Install BAOFitter: git clone https://gitlab.com/esarpa1/BAOFit/, cd BAOFit, pip install .
   


## Examples

Provide examples of how to use the BeXiCov package with code snippets and explanations.

## Code Description

- Generates the theoretical covariance matrix based on 1st order Lagrangian perturbation theory (Padmanabhan et al. 2009, Sarpa et al. 2023).
- Models the two-point correlation function multipoles using the damped correlation function model and polynomial broad-band (Ross et al. 2017).
- Computes the Hankel transform of the best-fit 2PCF to obtain the best-fit anisotropic power spectrum.
- Generates the Gaussian covariance matrix from the best-fit anisotropic power spectrum.

## License
MIT License

## Credits
This code has been developped by Elena Sarpa with the support of:
- Giosué Gambardella: Powerspectrum covariance
- Gaussian implementation used in Powerspectrum covariance has been performed using part of the
   comet-emu software https://gitlab.com/aegge/comet-emu by A. Eggmeier
